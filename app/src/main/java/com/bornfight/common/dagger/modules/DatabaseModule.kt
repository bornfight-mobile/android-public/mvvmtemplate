package com.bornfight.common.dagger.modules

import android.content.Context
import androidx.room.Room
import com.bornfight.common.data.database.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by lleopoldovic on 13/09/2019.
 */

@Module
class DatabaseModule {

    /**
     * Database initialization.
     * Set database name and (optionally) provide a fallback strategy here.
     */
    @Singleton
    @Provides
    fun database(context: Context): AppDatabase {
        return Room.databaseBuilder(
            context,
            AppDatabase::class.java, "database.db"
        )
            .fallbackToDestructiveMigration()
            .build()
    }
}
package com.bornfight.common.dagger.modules

import com.bornfight.common.mvvm.BaseFragment
import com.bornfight.demo.weather.intro.IntroFragment
import com.bornfight.demo.weather.news.NewsFragment
import com.bornfight.demo.weather.searchsuggestions.SearchSuggestionsFragment
import com.bornfight.demo.weather.weather.WeatherFragment
import com.bornfight.demo.weather.weather.WeatherViewPagerFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by lleopoldovic on 23/09/2019.
 */

@Module
abstract class BindingModuleFragments {

    // Bind every new fragment here, following the BaseFragment example:
    @ContributesAndroidInjector
    abstract fun contributeBaseFragment(): BaseFragment

    @ContributesAndroidInjector
    abstract fun contributeIntroIntroFragment(): IntroFragment

    @ContributesAndroidInjector
    abstract fun contributeIntroFragment(): SearchSuggestionsFragment

    @ContributesAndroidInjector
    abstract fun contributeCurrentWeatherFragment(): WeatherFragment

    @ContributesAndroidInjector
    abstract fun contributeWeatherViewPagerFragment(): WeatherViewPagerFragment

    @ContributesAndroidInjector
    abstract fun contributeNewsFragment(): NewsFragment

}
package com.bornfight.common.mvvm.basemodels

enum class ResponseCodes(val code: Int) {
    UNDEFINED(0),
    UNAUTHORIZED(401)
}
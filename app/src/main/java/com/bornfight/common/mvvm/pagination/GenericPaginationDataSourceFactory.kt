package com.bornfight.common.mvvm.pagination

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.bornfight.common.mvvm.basemodels.ResourceState
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by lleopoldovic on 23/01/2020.
 *
 * Use this class to create a pagination DataSourceFactory.
 *
 * The generic DataType stands for the type of items which will be fetched for pagination (e.g. FeedItem,
 * EducationArticle etc.)
 * The generic PaginationParams stands for the pagination params class which extends [GenericPaginationParams].
 *
 * @param params Extend [GenericPaginationParams] and provide all the other params which need to be used for fetching
 *               the data (e.g. search query, unique identifiers etc.).
 * @param call A repository function responsible for fetching the pagination data. It should accept @param params as
 *             the argument.
 *
 */

class GenericPaginationDataSourceFactory<DataType, PaginationParams> (
    var params: GenericPaginationParams<PaginationParams>,
    var call: (params: GenericPaginationParams<PaginationParams>) -> Observable<ResourceState<List<DataType>>>,
    var compositeDisposable: CompositeDisposable
) : DataSource.Factory<Int, DataType>() {

    private var mParams: GenericPaginationParams<PaginationParams> = params
    private var dataSource: GenericPaginationDataSource<DataType, PaginationParams>? = null

    override fun create(): DataSource<Int, DataType> {
        // A new instance of _dataSource has to be created every time create() is called.
        val _dataSource = GenericPaginationDataSource<DataType, PaginationParams>(mParams, call, compositeDisposable)
        dataSource = _dataSource

        return _dataSource
    }

    /**
     * When using the Paging Library, it's up to the data layer to notify the other layers of your app when a table or
     * row has become stale. To do so, call invalidate() from the DataSource class that you've chosen for your app.
     *
     * Note: Your app's UI can trigger this data invalidation functionality using a swipe to refresh model.
     */
    fun reInit(params: GenericPaginationParams<PaginationParams>) {
        this.mParams = params
        dataSource?.invalidate()
    }

    /**
     * Use this method to retry the loading of a failed page (either initial page, next page, or before page).
     */
    fun retry() {
        dataSource?.retry()
    }

    /**
     * This method is only used by the [BasePaginationViewModel] to expose the state of the call (success/loading/error)
     * to wrap the result of the pagination call with [ResourceState].
     */
    fun getFetchState(): MutableLiveData<ResourceState<DataType>> {
        return dataSource?.getFetchState() ?: MutableLiveData()
    }
}
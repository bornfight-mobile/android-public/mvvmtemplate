package com.bornfight.demo.weather.searchsuggestions

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.bornfight.common.mvvm.BaseViewModel
import com.bornfight.common.mvvm.basemodels.ResourceState
import com.bornfight.common.scheduler.SchedulerProvider
import com.bornfight.demo.weather.model.SearchSuggestion
import com.bornfight.demo.weather.repository.searchsuggestion.SearchSuggestionRepo
import javax.inject.Inject

/**
 * Created by lleopoldovic on 13/09/2019.
 */

// Check this article to avoid common ViewModel mistakes:
// https://proandroiddev.com/5-common-mistakes-when-using-architecture-components-403e9899f4cb

class SearchSuggestionsViewModel @Inject constructor(
    schedulers: SchedulerProvider,
    private val searchSuggestionRepo: SearchSuggestionRepo
) : BaseViewModel(schedulers) {

    // View should never be able to update viewModel. ViewModel should always update the view.
    // Therefore, we encapsulate the data like this:
    private val _searchSuggestions = MutableLiveData<ResourceState<List<SearchSuggestion>>>()
    val searchSuggestions: LiveData<ResourceState<List<SearchSuggestion>>> = _searchSuggestions

    // Initialize the data like this, rather than calling the function from the view's onCreate() or
    // onResume().
    // That way, we keep up to viewModel_should_always_provide_view_with_data philosophy and avoid
    // reloading the data after every screen rotation.
    init {
        loadSearchSuggestions()
    }

    private fun loadSearchSuggestions() {
        observableCall(liveData = _searchSuggestions, call = {
            searchSuggestionRepo.getSearchSuggestions()
        })
    }

    fun saveSearchSuggestion(searchSuggestion: SearchSuggestion) {
        completableCallFromAction(_searchSuggestions, call = {
            searchSuggestionRepo.saveSearchSuggestion(searchSuggestion)
        })
    }
}
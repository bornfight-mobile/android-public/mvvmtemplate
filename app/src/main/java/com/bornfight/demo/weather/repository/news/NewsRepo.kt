package com.bornfight.demo.weather.repository.news

import com.bornfight.common.mvvm.basemodels.ResourceState
import com.bornfight.common.mvvm.pagination.GenericPaginationParams
import com.bornfight.demo.weather.model.News
import com.bornfight.demo.weather.model.pagination.NewsPaginationParams
import io.reactivex.Observable

interface NewsRepo {

    // TODO: Add generic params
    fun getNewss(params: NewsPaginationParams): Observable<ResourceState<List<News>>>
    fun saveNewss(items: List<News>)
    fun deleteNewss(items: List<News>)

}
package com.bornfight.demo.weather.weather

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.bornfight.appname.R
import com.bornfight.common.mvvm.BaseFragment
import com.bornfight.common.mvvm.ResourceStateObserver
import com.bornfight.demo.weather.model.Forecast
import com.bornfight.demo.weather.model.WeatherResponse
import com.bornfight.demo.weather.weather.adapters.ForecastAdatper
import com.bornfight.utils.kelvinToCelsiusString
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_weather.*

class WeatherFragment : BaseFragment() {

    private val viewModel: WeatherViewModel by viewModels { viewModelFactory }
    private val forecastAdapter = ForecastAdatper()

    private val cityName: String by lazy { arguments?.getString(ARG_CITY, "Zagreb") ?: "Zagreb" }
    private val date: Long by lazy {
        arguments?.getLong(ARG_DATE, System.currentTimeMillis()) ?: System.currentTimeMillis()
    }
    private val isCurrent by lazy { arguments?.getBoolean(ARG_IS_CURRENT, true) ?: true }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_weather, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bind()

        forecastRV.apply {
            adapter = forecastAdapter
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        }

        if (isCurrent) {
            viewModel.getCurrentWeatherData(cityName)
        }

        viewModel.getForecastData(cityName, date)

    }

    private fun bind() {
        if (isCurrent) {
            viewModel.weatherData.observe(viewLifecycleOwner, ResourceStateObserver(this,
                onSuccess = { populateUi(it ?: return@ResourceStateObserver) }
            ))
        }

        viewModel.forecastData.observe(viewLifecycleOwner, ResourceStateObserver(this, {
            forecastAdapter.setItems(it ?: listOf())
            if (!isCurrent) {
                populateUi(it ?: listOf())
            }
        }))
    }

    private fun populateUi(weatherResponse: WeatherResponse) {
        Glide.with(weatherIconIV.context).load(weatherResponse.getImageUrl()).into(weatherIconIV)
        dateTV.text = weatherResponse.getDateTimeString()
        temperatureTV.text = weatherResponse.main.temp.kelvinToCelsiusString()
        locationTV.text = weatherResponse.name
        messageTV.text = weatherResponse.weather[0].description
    }

    private fun populateUi(forecasts: List<Forecast>) {
        if (forecasts.isNullOrEmpty()) return
        tempMax.visibility = View.VISIBLE
        tempMin.visibility = View.VISIBLE
        temperatureTV.visibility = View.GONE

        val maxTemp = forecasts.maxBy { it.main.maxTemp }?.main?.maxTemp ?: 0.0
        val minTemp = forecasts.minBy { it.main.minTemp }?.main?.minTemp ?: 0.0
        val message = forecasts[forecasts.size / 2].weather[0].description
        val image = forecasts[forecasts.size / 2].getImageUrl()

        Glide.with(weatherIconIV.context).load(image).into(weatherIconIV)
        dateTV.text = forecasts[0].getDateString()
        tempMin.text = minTemp.kelvinToCelsiusString()
        tempMax.text = maxTemp.kelvinToCelsiusString()
        messageTV.text = message
        locationTV.text = forecasts.first().name
    }

    companion object {

        const val ARG_CITY = "city"
        const val ARG_DATE = "date"
        const val ARG_IS_CURRENT = "is_current"

        @JvmStatic
        fun newInstance(cityName: String, date: Long, isCurrent: Boolean) =
            WeatherFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_CITY, cityName)
                    putLong(ARG_DATE, date)
                    putBoolean(ARG_IS_CURRENT, isCurrent)
                }
            }
    }
}

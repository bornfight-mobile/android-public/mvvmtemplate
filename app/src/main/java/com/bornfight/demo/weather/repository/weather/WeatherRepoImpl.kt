package com.bornfight.demo.weather.repository.weather

import com.bornfight.common.data.database.AppDatabase
import com.bornfight.common.data.retrofit.ApiInterface
import com.bornfight.common.mvvm.BaseRepo
import com.bornfight.common.mvvm.basemodels.ResourceState
import com.bornfight.common.session.DevicePreferences
import com.bornfight.common.session.SessionPrefImpl
import com.bornfight.demo.weather.model.Forecast
import com.bornfight.demo.weather.model.WeatherResponse
import io.reactivex.Observable
import javax.inject.Inject

class WeatherRepoImpl @Inject constructor(
    private val database: AppDatabase,
    private val api: ApiInterface,
    private val session: SessionPrefImpl,
    private val devicePrefs: DevicePreferences
) : BaseRepo(database, api, session, devicePrefs), WeatherRepo {

    override fun getCurrentWeatherForCity(cityName: String): Observable<ResourceState<WeatherResponse>> {
        return oneSideCall(
            call = { api.getWeatherForCity(cityName) },
            saveCallData = { database.weatherDao().saveWeatherData(it) },
            performOnError = { database.weatherDao().getWeatherData(cityName) }
        )
    }

    override fun getForecastForCity(cityName: String): Observable<ResourceState<List<Forecast>>> {
        return oneSideCall({
            api.getForecastForCity(cityName).map {
                it.forecastList.map {
                    it.copy(name = cityName)
                }
            }
        }, {
            database.weatherDao().saveForecastData(it)
        }, {
            database.weatherDao().getForecastData(cityName)
        })
    }
}

